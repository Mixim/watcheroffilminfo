using System;
using System.Net;
using System.IO;
using System.Security;
using Common;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Подключение к хранилищу данных с помощью API.
	/// </summary>
	public class ConnectorToDatastore
	{
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА ConnectorToDatastore.
			
			/// <summary>
			/// Разделитель в URL.
			/// </summary>
			protected const String URLsSeparator="/";

			/// <summary>
			/// Разделитель команды и аргументов.
			/// </summary>
			protected const String URLsCommandAndArgumentSeparator="?";

			/// <summary>
			/// Символ для присвоение аргументу значения.
			/// </summary>
			protected const String URLsArgumentAssignment="=";

			/// <summary>
			/// Разделитель аргументов.
			/// </summary>
			protected const String URLsArgumentSeparator="&";

		#endregion

		#region Необходимые поля и свойства класса.

			/// <summary>
			/// Url хранилища.
			/// </summary>
			public String DatastoresURL{ get; set; }

			/// <summary>
			/// Ключ разработчика.
			/// </summary>
			public String ApiKey{ get; set; }

		#endregion

		/// <summary>
		/// Инициализация класса <see cref="WatcherOfFilmInfoLib.ConnectorToDatastore"/>.
		/// </summary>
		public ConnectorToDatastore ()
		{
		}

		public ConnectorToDatastore (string DatastoresURL, string ApiKey)
		{
			this.DatastoresURL = DatastoresURL;
			this.ApiKey = ApiKey;
		}

		/// <summary>
		/// Выполнить API-команду.
		/// </summary>
		/// <returns>Ответ хранилища на команду.</returns>
		/// <param name="DatastoresURL">Url хранилища.</param>
		/// <param name="Command">Команда.</param>
		/// <param name="ApiKey">Ключ разработчика.</param>
		/// <param name="TypeOfReturnedValueByAPI">Тип возвращаемого значения.</param>
		/// <param name="ArgumentsOfCommand">Аргументы команды.</param>
		public String ExecuteAPI(String DatastoresURL, CinemateCcAPICommands Command, String ApiKey, TypesOfReturnedValueByAPI TypeOfReturnedValueByAPI, params ArgumentOfCinemateCcApiCommands[] ArgumentsOfCommand)
		{
			String urlForRequest;
			HttpWebResponse response;
			String returnedValue;

			urlForRequest=this.GenerateURL(DatastoresURL, Command, ApiKey, TypeOfReturnedValueByAPI, ArgumentsOfCommand);

			response=this.SendWebRequestToServer(urlForRequest);

			using(Stream dataStream=response.GetResponseStream())
			{
				using(StreamReader reader=new StreamReader(dataStream))
				{
					returnedValue=reader.ReadToEnd();
				}
			}

			response.Close();

			return returnedValue;
		}

		/// <summary>
		/// Выполнить API-команду.
		/// </summary>
		/// <returns>Десериализованный результат исполнения команды.</returns>
		/// <param name="Command">Команда.</param>
		/// <param name="TypeOfReturnedValueByAPI">Тип возвращаемого значения.</param>
		/// <param name="ArgumentsOfCommand">Аргументы команды.</param>
		/// <typeparam name="T">Тип результата.</typeparam>
		public T ExecuteAPI<T>(CinemateCcAPICommands Command, TypesOfReturnedValueByAPI TypeOfReturnedValueByAPI, params ArgumentOfCinemateCcApiCommands[] ArgumentsOfCommand) where T:ISerializationBase, new()
		{
			string serializedT;
			serializedT = this.ExecuteAPI (this.DatastoresURL, Command, this.ApiKey, TypeOfReturnedValueByAPI, ArgumentsOfCommand);
			T returnedValue = new T ();
			returnedValue.Deserialize (serializedT);
			return returnedValue;
		}

		/// <summary>
		/// Сгенерировать URL для выполнения API-команды на сервере.
		/// </summary>
		/// <returns>Url.</returns>
		/// <param name="DatastoresURL">Url хранилища.</param>
		/// <param name="Command">Команда.</param>
		/// <param name="ApiKey">Ключ разработчика.</param>
		/// <param name="TypeOfReturnedValueByAPI">Тип возвращаемого значения.</param>
		/// <param name="ArgumentsOfCommand">Аргументы команды.</param>
		protected String GenerateURL(String DatastoresURL, CinemateCcAPICommands Command, String ApiKey, TypesOfReturnedValueByAPI TypeOfReturnedValueByAPI, params ArgumentOfCinemateCcApiCommands[] ArgumentsOfCommand)
		{
			String returnedValue=String.Empty;

			//получаем что-то вроде "http://api.cinemate.cc/" (т.е. вставляем разделитель между URL сайта и дальнейшей командой)
			returnedValue=DatastoresURL.EndsWith(ConnectorToDatastore.URLsSeparator) == true ? DatastoresURL : DatastoresURL + ConnectorToDatastore.URLsSeparator;

			//получаем "http://api.cinemate.cc/movie.search"
			returnedValue+=EnumsUtilities.GetStringValue(Command);

			//получаем "http://api.cinemate.cc/movie.search?"
			returnedValue+=ConnectorToDatastore.URLsCommandAndArgumentSeparator;

			//получаем "http://api.cinemate.cc/movie.search?apikey"
			returnedValue+=EnumsUtilities.GetStringValue(NamesOfCinemateCcAPICommandsArguments.ApiKey);

			//получаем "http://api.cinemate.cc/movie.search?apikey="
			returnedValue+=ConnectorToDatastore.URLsArgumentAssignment;

			//получаем "http://api.cinemate.cc/movie.search?apikey=APIKEY"
			returnedValue+=ApiKey;

			//получаем "http://api.cinemate.cc/movie.search?apikey=APIKEY&term=Пираты%20кариб..."
			if(ArgumentsOfCommand!=null)
			{
				foreach(var argument in ArgumentsOfCommand)
				{
					returnedValue+=ConnectorToDatastore.URLsArgumentSeparator+EnumsUtilities.GetStringValue(argument.ArgumentsType)+ConnectorToDatastore.URLsArgumentAssignment+argument.ArgumentsValue;
				}
			}

			//получаем "http://api.cinemate.cc/movie.search?apikey=APIKEY&term=Пираты%20кариб...&format=xml"
			if(TypeOfReturnedValueByAPI!=TypesOfReturnedValueByAPI.Default)
			{
				returnedValue += ConnectorToDatastore.URLsArgumentSeparator + EnumsUtilities.GetStringValue (NamesOfCinemateCcAPICommandsArguments.Format) + ConnectorToDatastore.URLsArgumentAssignment + EnumsUtilities.GetStringValue (TypeOfReturnedValueByAPI);
			}


			return returnedValue;
		}

		/// <summary>
		/// Отправить web-запрос на сервер.
		/// </summary>
		/// <returns>Ответ сервера на запрос.</returns>
		/// <param name="URLForRequest">Url для запроса.</param>
		protected HttpWebResponse SendWebRequestToServer(String URLForRequest)
		{
			HttpWebResponse returnedValue;
			HttpWebRequest request;

			request= HttpWebRequest.Create(URLForRequest) as HttpWebRequest;

			request.Credentials=CredentialCache.DefaultCredentials;

			returnedValue=request.GetResponse() as HttpWebResponse;

			return returnedValue;
		}
	}
}

