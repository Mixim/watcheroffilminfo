using System;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Представление имени аргумента команды и его значения.
	/// </summary>
	public class ArgumentOfCinemateCcApiCommands
	{
		#region Свойства.
			
			/// <summary>
			/// Имя аргумента API-команды.
			/// </summary>
			public NamesOfCinemateCcAPICommandsArguments ArgumentsType{get;set;}

			/// <summary>
			/// Значение аргумента API-команды.
			/// </summary>
			public String ArgumentsValue{get;set;}

		#endregion

		/// <summary>
		/// Инициализирует класс <see cref="WatcherOfFilmInfoLib.ArgumentOfCinemateCcApiCommands"./>.
		/// </summary>
		/// <param name="ArgumentsType">Имя аргумента API-команды.</param>
		/// <param name="ArgumentsValue">Значение аргумента API-команды.</param>
		public ArgumentOfCinemateCcApiCommands (NamesOfCinemateCcAPICommandsArguments ArgumentsType, String ArgumentsValue)
		{
			this.ArgumentsType=ArgumentsType;
			this.ArgumentsValue=ArgumentsValue;
		}
	}
}

