using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Информация о найденных персонах.
	/// </summary>
	[DataContract]
	public class PersonSearchInfo:ISerializationBase
	{
		/// <summary>
		/// Найденные фильмы.
		/// </summary>
		[DataMember(Name = "person")]
		public List<PersonGenericInfo> Persons { get; set; }

		/// <summary>
		/// Сериализовать информацию.
		/// </summary>
		public string Serialize()
		{
			string returnedValue;

			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(PersonSearchInfo));
			using (MemoryStream stream = new MemoryStream()) 
			{
				jsonSerializer.WriteObject (stream, this);
				stream.Position = 0;
				using (StreamReader reader = new StreamReader(stream)) 
				{
					returnedValue = reader.ReadToEnd ();
				}
			}

			return returnedValue;
		}

		/// <summary>
		/// Десериализовать информацию.
		/// </summary>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		public void Deserialize(string SerializedObject)
		{
			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(PersonSearchInfo));

			using (MemoryStream stream = new MemoryStream()) 
			{
				using (StreamWriter writer = new StreamWriter(stream)) 
				{
					writer.Write (NormalizeSerializedObject(SerializedObject));
					writer.Flush ();
					stream.Position = 0;
					this.Init((PersonSearchInfo)jsonSerializer.ReadObject (stream));
				}
			}
		}

		/// <summary>
		/// Нормализовать сериализованный объект.
		/// </summary>
		/// <returns>Нормализованный сериализованный объект.</returns>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		protected static string NormalizeSerializedObject(string SerializedObject)
		{
			string returnedValue;
			if (!Regex.IsMatch (SerializedObject, "(?<=person\":) \\[\\{(.*?)\\.*\\]\\}$")) 
			{
				returnedValue = Regex.Replace (SerializedObject, "(?<=person\":) \\{(.*?)\\.*\\}$", " [{$1]}");
			}
			else 
			{
				returnedValue = SerializedObject;
			}

			return returnedValue;
		}

		/// <summary>
		/// Инициализировать свойства с информацией о фильме.
		/// </summary>
		/// <param name="InfoForInitialization">Информация для инициализации.</param>
		protected void Init(PersonSearchInfo InfoForInitialization)
		{
			this.Persons = InfoForInitialization.Persons;
		}
	}
}

