using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Человек.
	/// </summary>
	[DataContract]
	public class Person
	{
		#region Внутренние типы и классы.

			/// <summary>
			/// Атрибуты.
			/// </summary>
			[DataContract]
			protected class Attrib
			{
				[DataMember(Name = "id")]
				public string Id { get; set; }
			}

		#endregion

		/// <summary>
		/// Атрибуты человека.
		/// </summary>
		/// <value>The attributes.</value>
		[DataMember(Name = "attrib")]
		protected Attrib Attributes { get; set; }

		/// <summary>
		/// Идентификатор.
		/// </summary>
		public string Id
		{
			get 
			{
				return this.Attributes.Id;
			}
			set 
			{
				if (this.Attributes == null) 
				{
					this.Attributes = new Attrib ();
				}
				this.Attributes.Id = value;
			}
		}

		/// <summary>
		/// Имя.
		/// </summary>
		/// <value>The name.</value>
		[DataMember(Name = "value")]
		public string Name { get; set; }

		public override string ToString ()
		{
			return this.Name;
		}
	}


	/// <summary>
	/// URL-тег.
	/// </summary>
	[DataContract]
	public class UrlTag
	{
		/// <summary>
		/// URL.
		/// </summary>
		[DataMember(Name = "url")]
		public string Url { get; set; }
	}

	/// <summary>
	/// Популярность фильма.
	/// </summary>
	[DataContract]
	public class RatingInfo
	{
		/// <summary>
		/// Рейтинг.
		/// </summary>
		[DataMember(Name = "rating", IsRequired = true)]
		public string Rating { get; set; }

		/// <summary>
		/// Количество голосов.
		/// </summary>
		[DataMember(Name = "votes", IsRequired = true)]
		public string Votes { get; set; }
	}

	/// <summary>
	/// Режиссер.
	/// </summary>
	[DataContract]
	public class Director
	{
		/// <summary>
		/// Информация.
		/// </summary>
		[DataMember(Name = "person")]
		public Person Person { get; set; }

		public override string ToString ()
		{
			return this.Person.Name;
		}
	}

	/// <summary>
	/// Постеры.
	/// </summary>
	[DataContract]
	public class Posters
	{
		/// <summary>
		/// Ссылка на постер небольшого размера.
		/// </summary>
		[DataMember(Name = "small")]
		public UrlTag Small { get; set; }

		/// <summary>
		/// Ссылка на постер среднего размера.
		/// </summary>
		[DataMember(Name = "medium")]
		public UrlTag Medium { get; set; }

		/// <summary>
		/// Ссылка на постер большого размера.
		/// </summary>
		[DataMember(Name = "big")]
		public UrlTag Big { get; set; }
	}

	/// <summary>
	/// Список стран.
	/// </summary>
	[DataContract]
	public class Countries
	{
		/// <summary>
		/// Наименования стран.
		/// </summary>
		[DataMember(Name = "name")]
		public List<string> Names { get; set; }

		public override string ToString ()
		{
			if (this.Names != null) 
			{
				return string.Join (", ", this.Names);
			}
			else 
			{
				return string.Empty;
			}
		}
	}

	/// <summary>
	/// Актерский состав.
	/// </summary>
	[DataContract]
	public class Cast
	{
		/// <summary>
		/// Информация об актерах.
		/// </summary>
		[DataMember(Name = "person")]
		public List<Person> Actors { get; set; }

		public override string ToString ()
		{
			if (this.Actors != null)
			{
				return string.Join (", ", this.Actors);
			}
			else 
			{
				return string.Empty;
			}
		}
	}

	/// <summary>
	/// Жанры фильма.
	/// </summary>
	[DataContract]
	public class Genre
	{
		/// <summary>
		/// Информация.
		/// </summary>
		/// <value>The genres.</value>
		[DataMember(Name = "name")]
		public List<string> Genres { get; set; }

		public override string ToString ()
		{
			if (this.Genres != null)
			{
				return string.Join (", ", this.Genres);
			}
			else
			{
				return string.Empty;
			}
		}
	}

	/// <summary>
	/// Основная информация о фильме.
	/// </summary>
	[DataContract]
	public class MovieGenericInfo
	{
		/// <summary>
		/// Id фильма.
		/// </summary>
		[DataMember(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Название фильма в оригинале.
		/// </summary>
		[DataMember(Name = "title_original")]
		public string OriginalTitle { get; set; }

		/// <summary>
		/// Русскоязычное название фильма.
		/// </summary>
		[DataMember(Name = "title_russian")]
		public string RussianTitle { get; set; }

		/// <summary>
		/// Постеры.
		/// </summary>
		[DataMember(Name = "poster")]
		public Posters PosterLinks { get; set; }

		/// <summary>
		/// Тип фильма.
		/// </summary>
		[DataMember(Name = "type")]
		public string Type { get; set; }

		/// <summary>
		/// Длительность фильма в минутах.
		/// </summary>
		[DataMember(Name = "runtime")]
		public int Runtime { get; set; }

		/// <summary>
		/// Год выхода фильма.
		/// </summary>
		[DataMember(Name = "year")]
		public int Year { get; set; }

		/// <summary>
		/// Рейтинг фильма по версии http://kinopoisk.ru.
		/// </summary>
		[DataMember(Name = "kinopoisk")]
		public RatingInfo Kinopoisk { get; set; }

		/// <summary>
		/// Рейтинг фильма по версии http://imdb.com.
		/// </summary>
		[DataMember(Name = "imdb")]
		public RatingInfo Imdb { get; set; }

		/// <summary>
		/// Ссылка на страницу фильма.
		/// </summary>
		[DataMember(Name = "url")]
		public string Url { get; set; }

		public override string ToString ()
		{
			return string.IsNullOrEmpty (this.RussianTitle) ? this.OriginalTitle : this.RussianTitle;
		}
	}

	/// <summary>
	/// Найденный фильм.
	/// </summary>
	[DataContract]
	public class DeterminedMovie
	{
		/// <summary>
		/// Id фильма.
		/// </summary>
		[DataMember(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Название фильма в оригинале.
		/// </summary>
		[DataMember(Name = "title_original")]
		public string OriginalTitle { get; set; }

		/// <summary>
		/// Русскоязычное название фильма.
		/// </summary>
		[DataMember(Name = "title_russian")]
		public string RussianTitle { get; set; }

		/// <summary>
		/// Постеры.
		/// </summary>
		[DataMember(Name = "poster")]
		public Posters PosterLinks { get; set; }

		/// <summary>
		/// Тип фильма.
		/// </summary>
		[DataMember(Name = "type")]
		public string Type { get; set; }

		/// <summary>
		/// Год выхода фильма.
		/// </summary>
		[DataMember(Name = "year")]
		public int Year { get; set; }

		/// <summary>
		/// Ссылка на страницу фильма.
		/// </summary>
		[DataMember(Name = "url")]
		public string Url { get; set; }
	}

	/// <summary>
	/// Фотографии.
	/// </summary>
	[DataContract]
	public class Photos:Posters
	{
	}

	/// <summary>
	/// Основная информация о персоне.
	/// </summary>
	[DataContract(Name="person")]
	public class PersonGenericInfo
	{
		/// <summary>
		/// Id персоны.
		/// </summary>
		[DataMember(Name = "id")]
		public int Id { get; set; }

		/// <summary>
		/// Имя персоны в оригинале.
		/// </summary>
		[DataMember(Name = "name_original")]
		public string OriginalName { get; set; }

		/// <summary>
		/// Русскоязычное имя персоны.
		/// </summary>
		[DataMember(Name = "name")]
		public string Name { get; set; }

		/// <summary>
		/// 3 тега со ссылками на фотографии разных размеров.
		/// </summary>
		[DataMember(Name = "photo")]
		public Photos PhotosLinks { get; set; }

		/// <summary>
		/// Ссылка на страницу персоны.
		/// </summary>
		[DataMember(Name = "url")]
		public string Url { get; set; }
	}

	[DataContract]
	public class MoviesInfoForPerson
	{
		#region Внутренние типы и классы.

			/// <summary>
			/// Обертка над фильмами персоны.
			/// </summary>
			[DataContract]
			protected class RootObject
			{
				[DataMember(Name = "movie")]
				public List<MovieGenericInfo> Movie{ get; set; }
			}

		#endregion

		[DataMember(Name = "actor")]
		protected RootObject whereActor{ get; set; }

		/// <summary>
		/// Фильмы, в которых персона была актером.
		/// </summary>
		public List<MovieGenericInfo> WhereActor
		{
			get 
			{
				if (this.whereActor != null) 
				{
					return this.whereActor.Movie;
				}
				else 
				{
					return null;
				}
			}
			set 
			{
				if (this.whereActor == null) 
				{
					this.whereActor = new RootObject ();
				}
				this.whereActor.Movie = value;
			}
		}

		[DataMember(Name = "director")]
		protected RootObject whereDirector{ get; set; }

		/// <summary>
		/// Фильмы, в которых персона была режиссером.
		/// </summary>
		public List<MovieGenericInfo> WhereDirector
		{
			get 
			{
				if (this.whereDirector != null) 
				{
					return this.whereDirector.Movie;
				}
				else 
				{
					return null;
				}
			}
			set 
			{
				if (this.whereDirector == null) 
				{
					this.whereDirector = new RootObject ();
				}
				this.whereDirector.Movie = value;
			}
		}
	}
}

