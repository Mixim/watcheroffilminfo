using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Основная информация о персоне.
	/// </summary>
	[DataContract(Name="person")]
	public class PersonInfo:ISerializationBase
	{
		#region Внутренние типы и классы.

			/// <summary>
			/// Объект, необходимый для сериализации\десериализации информации о персоне.
			/// </summary>
			[DataContract]
			protected class RootObject
			{
				[DataMember(Name="person")]
				public PersonInfo Person { get; set; }
			}

		#endregion

		#region Свойства.

			/// <summary>
			/// Id персоны.
			/// </summary>
			[DataMember(Name = "id")]
			public int Id { get; set; }

			/// <summary>
			/// Имя персоны в оригинале.
			/// </summary>
			[DataMember(Name = "name_original")]
			public string OriginalName { get; set; }

			/// <summary>
			/// Русскоязычное имя персоны.
			/// </summary>
			[DataMember(Name = "name")]
			public string Name { get; set; }

			/// <summary>
			/// 3 тега со ссылками на фотографии разных размеров.
			/// </summary>
			[DataMember(Name = "photo")]
			public Photos PhotosLinks { get; set; }

			/// <summary>
			/// Ссылка на страницу персоны.
			/// </summary>
			[DataMember(Name = "url")]
			public string Url { get; set; }

		#endregion

		/// <summary>
		/// Сериализовать информацию.
		/// </summary>
		public string Serialize()
		{
			string returnedValue;
			RootObject wrapper = new RootObject ();
			wrapper.Person = this;

			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(RootObject));
			using (MemoryStream stream = new MemoryStream()) 
			{
				jsonSerializer.WriteObject (stream, wrapper);
				stream.Position = 0;
				using (StreamReader reader = new StreamReader(stream)) 
				{
					returnedValue = reader.ReadToEnd ();
				}
			}

			return returnedValue;
		}

		/// <summary>
		/// Десериализовать информацию.
		/// </summary>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		public void Deserialize(string SerializedObject)
		{
			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(RootObject));

			using (MemoryStream stream = new MemoryStream()) 
			{
				using (StreamWriter writer = new StreamWriter(stream)) 
				{
					writer.Write (SerializedObject);
					writer.Flush ();
					stream.Position = 0;
					RootObject wrapper = (RootObject)jsonSerializer.ReadObject (stream);
					this.Init(wrapper.Person);
				}
			}
		}

		/// <summary>
		/// Инициализировать свойства с информацией о персоне.
		/// </summary>
		/// <param name="InfoForInitialization">Информация для инициализации.</param>
		protected void Init(PersonInfo InfoForInitialization)
		{
			this.Id = InfoForInitialization.Id;
			this.Name = InfoForInitialization.Name;
			this.OriginalName = InfoForInitialization.OriginalName;
			this.PhotosLinks = InfoForInitialization.PhotosLinks;
			this.Url = InfoForInitialization.Url;
		}
	}
}

