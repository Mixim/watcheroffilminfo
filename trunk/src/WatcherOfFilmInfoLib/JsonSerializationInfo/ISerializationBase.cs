using System;

namespace WatcherOfFilmInfoLib
{
	public interface ISerializationBase
	{
		/// <summary>
		/// Сериализовать информацию.
		/// </summary>
		string Serialize();

		/// <summary>
		/// Десериализовать информацию.
		/// </summary>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		void Deserialize(string SerializedObject);
	}
}

