using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Информация о фильме.
	/// </summary>
	[DataContract(Name="movie")]
	public class MovieInfo:ISerializationBase
	{
		#region Внутренние типы и классы.

			/// <summary>
			/// Объект, необходимый для сериализации\десериализации информации о фильме.
			/// </summary>
			[DataContract]
			protected class RootObject
			{
				[DataMember(Name="movie")]
				public MovieInfo Movie { get; set; }
			}

		#endregion

		#region Свойства.

			/// <summary>
			/// Id фильма.
			/// </summary>
			[DataMember(Name = "id")]
			public int Id { get; set; }

			/// <summary>
			/// Название фильма в оригинале.
			/// </summary>
			[DataMember(Name = "title_original")]
			public string OriginalTitle { get; set; }

			/// <summary>
			/// Русскоязычное название фильма.
			/// </summary>
			[DataMember(Name = "title_russian", IsRequired = false)]
			public string RussianTitle { get; set; }

			/// <summary>
			/// Режиссер фильма.
			/// </summary>
			[DataMember(Name = "director")]
			public Director Director { get; set; }

			private Cast cast = new Cast();
			/// <summary>
			/// Актеры фильма.
			/// </summary>
			[DataMember(Name = "cast")]
			public Cast Cast 
			{ 
				get 
				{
					return this.cast;
				}
				set 
				{
					if (value != null)
					{
						this.cast = value;
					}
				}
			}

			/// <summary>
			/// Страны-создатели фильма.
			/// </summary>
			[DataMember(Name = "country")]
			public Countries Countries { get; set; }

			/// <summary>
			/// 3 тега со ссылками на постеры разных размеров.
			/// </summary>
			[DataMember(Name = "poster")]
			public Posters PostersLinks { get; set; }

			/// <summary>
			/// Тип фильма.
			/// </summary>
			[DataMember(Name = "type")]
			public string Type { get; set; }

			/// <summary>
			/// Жанры фильма.
			/// </summary>
			[DataMember(Name = "genre")]
			public Genre Genre { get; set; }

			/// <summary>
			/// Длительность фильма в минутах.
			/// </summary>
			[DataMember(Name = "runtime")]
			public int Runtime { get; set; }

			/// <summary>
			/// Описание фильма.
			/// </summary>
			[DataMember(Name = "description", IsRequired = false)]
			public string Description { get; set; }

			/// <summary>
			/// Год выхода фильма.
			/// </summary>
			[DataMember(Name = "year", IsRequired = false)]
			public int Year { get; set; }

			/// <summary>
			/// Дата выхода фильма в мире в ISO формате.
			/// </summary>
			[DataMember(Name = "release_date_world")]
			public string WorldReleaseDate { get; set; }

			/// <summary>
			/// Дата выхода фильма в России в ISO формате.
			/// </summary>
			[DataMember(Name = "release_date_russia", IsRequired = false)]
			public string RussiaReleaseDate { get; set; }

			/// <summary>
			/// Рейтинг фильма по версии http://kinopoisk.ru.
			/// </summary>
			[DataMember(Name = "kinopoisk", IsRequired = false)]
			public RatingInfo Kinopoisk { get; set; }

			/// <summary>
			/// Рейтинг фильма по версии http://imdb.com.
			/// </summary>
			[DataMember(Name = "imdb", IsRequired = false)]
			public RatingInfo Imdb { get; set; }

			/// <summary>
			/// Ссылка на страницу фильма.
			/// </summary>
			[DataMember(Name = "url", IsRequired = false)]
			public string Url { get; set; }

			/// <summary>
			/// Ссылка на трейлер фильма.
			/// </summary>
			[DataMember(Name = "trailer", IsRequired = false)]
			public string Trailer { get; set; }

		#endregion

		/// <summary>
		/// Сериализовать информацию.
		/// </summary>
		public string Serialize()
		{
			string returnedValue;
			RootObject wrapper = new RootObject ();
			wrapper.Movie = this;

			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(RootObject));
			using (MemoryStream stream = new MemoryStream()) 
			{
				jsonSerializer.WriteObject (stream, wrapper);
				stream.Position = 0;
				using (StreamReader reader = new StreamReader(stream)) 
				{
					returnedValue = reader.ReadToEnd ();
				}
			}

			return returnedValue;
		}

		/// <summary>
		/// Десериализовать информацию.
		/// </summary>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		public void Deserialize(string SerializedObject)
		{
			DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(RootObject));

			using(MemoryStream stream = new MemoryStream ())
			{
				using (StreamWriter writer = new StreamWriter(stream)) 
				{
					writer.Write (NormalizeSerializedObject(SerializedObject));
					writer.Flush ();
					stream.Position = 0;
					RootObject wrapper = (RootObject)jsonSerializer.ReadObject (stream);
					this.Init (wrapper.Movie);
				}
			}
		}

		/// <summary>
		/// Нормализовать сериализованный объект.
		/// </summary>
		/// <returns>Нормализованный сериализованный объект.</returns>
		/// <param name="SerializedObject">Сериализованный объект.</param>
		protected string NormalizeSerializedObject(string SerializedObject)
		{
			string returnedValue;
			string strForProcessing;

			strForProcessing = SerializedObject;

			if (!Regex.IsMatch (strForProcessing, "(?<=country\": {\"name\":) \\[(.*?)\\]}")) 
			{
				strForProcessing = Regex.Replace (strForProcessing, "(?<=country\": {\"name\":)(.*?)}", "[$1]}");
			}

			if (!Regex.IsMatch (strForProcessing, "(?<=genre\": {\"name\":) \\[(.*?)\\]}")) 
			{
				strForProcessing = Regex.Replace (strForProcessing, "(?<=genre\": {\"name\":)(.*?)}", "[$1]}");
			}

			if (!Regex.IsMatch (strForProcessing, "(?<=cast\": {\"person\":) \\[(.*?)\\]}")) 
			{
				strForProcessing = Regex.Replace (strForProcessing, "(?<=cast\": {\"person\":)(.*?)}", "[$1]}");
			}

			returnedValue = strForProcessing;

			return returnedValue;
		}

		/// <summary>
		/// Инициализировать свойства с информацией о фильме.
		/// </summary>
		/// <param name="InfoForInitialization">Информация для инициализации.</param>
		protected void Init(MovieInfo InfoForInitialization)
		{
			this.Cast = InfoForInitialization.Cast;
			this.Countries = InfoForInitialization.Countries;
			this.Description = InfoForInitialization.Description;
			this.Director = InfoForInitialization.Director;
			this.Genre = InfoForInitialization.Genre;
			this.Id = InfoForInitialization.Id;
			this.Imdb = InfoForInitialization.Imdb;
			this.Kinopoisk = InfoForInitialization.Kinopoisk;
			this.OriginalTitle = InfoForInitialization.OriginalTitle;
			this.PostersLinks = InfoForInitialization.PostersLinks;
			this.Runtime = InfoForInitialization.Runtime;
			this.RussianTitle = InfoForInitialization.RussianTitle;
			this.RussiaReleaseDate = InfoForInitialization.RussiaReleaseDate;
			this.Trailer = InfoForInitialization.Trailer;
			this.Type = InfoForInitialization.Type;
			this.Url = InfoForInitialization.Url;
			this.WorldReleaseDate = InfoForInitialization.WorldReleaseDate;
			this.Year = InfoForInitialization.Year;
		}
	}
}

