using System;
using Common;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Имена аргументов API-команд.
	/// </summary>
	public enum NamesOfCinemateCcAPICommandsArguments
	{
		/// <summary>
		/// Логин пользователя.
		/// </summary>
		[StringValue("username")]
		Username=1,

		/// <summary>
		/// Пароль пользователя.
		/// </summary>
		[StringValue("password")]
		Password =2,

		/// <summary>
		/// PASSKEY пользователя.
		/// </summary>
		[StringValue("passkey")]
		Passkey=3,

		/// <summary>
		/// Формат возвращаемых сервером данных.
		/// </summary>
		[StringValue("format")]
		Format =4,

		/// <summary>
		/// Необходима только новая информация.
		/// </summary>
		[StringValue("newonly")]
		NewOnly =5,

		/// <summary>
		/// Ключ разработчика.
		/// </summary>
		[StringValue("apikey")]
		ApiKey=6,

		/// <summary>
		/// ID фильма.
		/// </summary>
		[StringValue("id")]
		Id =7,

		/// <summary>
		/// Тип фильмов.
		/// </summary>
		[StringValue("type")]
		Type =8,

		/// <summary>
		/// Cостояние фильма.
		/// </summary>
		[StringValue("state")]
		State =9,

		/// <summary>
		/// Специальный режим отображения лучших фильмов, отсортированных по рейтингу IMDB.
		/// </summary>
		[StringValue("mode")]
		Mode=10,

		/// <summary>
		/// Год выпуска фильма или сериала.
		/// </summary>
		[StringValue("year")]
		Year =11,

		/// <summary>
		/// Жанр.
		/// </summary>
		[StringValue("genre")]
		Genre =12,

		/// <summary>
		/// Отбор по стране.
		/// </summary>
		[StringValue("country")]
		Country =13,

		/// <summary>
		/// Критерий сортировки.
		/// </summary>
		[StringValue("order_by")]
		OrderBy =14,

		/// <summary>
		/// Порядок сортировки параметра order_by.
		/// </summary>
		[StringValue("order")]
		Order=15,

		/// <summary>
		/// Время начала среза параметра order_by в формате даты ДД.ММ.ГГГГ.
		/// </summary>
		[StringValue("from")]
		From =16,

		/// <summary>
		/// Время конца среза параметра order_by в формате даты ДД.ММ.ГГГГ.
		/// </summary>
		[StringValue("to")]
		To =17,

		/// <summary>
		/// Страница в выборке.
		/// </summary>
		[StringValue("page")]
		Page =18,

		/// <summary>
		/// Количество записей в выборке.
		/// </summary>
		[StringValue("per_page")]
		PerPage=19,

		/// <summary>
		/// Искомая строка.
		/// </summary>
		[StringValue("term")]
		Term =20
	}
}

