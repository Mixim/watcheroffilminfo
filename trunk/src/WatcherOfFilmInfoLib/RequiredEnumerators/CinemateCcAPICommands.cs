using System;
using Common;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// API-команды, доступные на сервере http://cinemate.cc.
	/// </summary>
	public enum CinemateCcAPICommands
	{
		/// <summary>
		/// Авторизация по логину и паролю.
		/// </summary>
		[StringValue("account.auth")]
		AccountAuthorization=1,

		/// <summary>
		/// Данные и статистика пользовательского аккаунта.
		/// </summary>
		[StringValue("account.profile")]
		AccountProfile=2,

		/// <summary>
		/// Записи ленты обновлений пользователя.
		/// </summary>
		[StringValue("account.updatelist")]
		AccountUpdateList=3,

		/// <summary>
		///  Список объектов слежения пользователя.
		/// </summary>
		[StringValue("account.watchlist")]
		AccountWatchlist=4,

		/// <summary>
		/// Информация о фильме.
		/// </summary>
		[StringValue("movie")]
		Movie=5,

		/// <summary>
		///  Результаты поиска фильмов, используя заданные фильтры. Возвращается 10 первых фильмов.
		/// </summary>
		[StringValue("movie.list")]
		MovieList=6,

		/// <summary>
		/// Поиск по заголовкам фильмов.
		/// </summary>
		[StringValue("movie.search")]
		MovieSearch=7,

		/// <summary>
		///  Основная информация о персоне.
		/// </summary>
		[StringValue("person")]
		Person=8,

		/// <summary>
		/// Информация о персоне, включая фильмы, в съемке которых персона принимала участие в качестве актера или режиссера. Основная информация о персоне идентична команде person.
		/// </summary>
		[StringValue("person.movies")]
		PersonMovies=9,

		/// <summary>
		/// Метод возвращает первые 10 результатов поиска по базе персон.
		/// </summary>
		[StringValue("person.search")]
		PersonSearch=10,

		/// <summary>
		/// Возвращает статистику сайта за последние сутки.
		/// </summary>
		[StringValue("stats.new")]
		StatsNew=11
	}
}

