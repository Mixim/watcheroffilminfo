using System;
using Common;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Формат значения, возвращаемого API-функцией.
	/// </summary>
	public enum TypesOfReturnedValueByAPI
	{
		/// <summary>
		/// XML-формат.
		/// </summary>
		[StringValue("xml")]
		XML=1,

		/// <summary>
		/// JSON-формат.
		/// </summary>
		[StringValue("json")]
		JSON=2,

		/// <summary>
		/// Формат, используемый на сервере по-умолчанию.
		/// </summary>
		[StringValue("")]
		Default
	}
}

