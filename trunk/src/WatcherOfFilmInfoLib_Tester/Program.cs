using System;
using WatcherOfFilmInfoLib;
using System.IO;

namespace WatcherOfFilmInfoLib_Tester
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			String datastoresURL = "http://api.cinemate.cc";
			ConnectorToDatastore connector = new ConnectorToDatastore (datastoresURL, AuthorizationConstants.ApiKeyForCinemate);
			CinemateCcAPICommands command = CinemateCcAPICommands.PersonSearch;
			TypesOfReturnedValueByAPI typeOfReturnedValueByAPI = TypesOfReturnedValueByAPI.JSON;
			ArgumentOfCinemateCcApiCommands[] argumentsOfCommand={new ArgumentOfCinemateCcApiCommands(NamesOfCinemateCcAPICommandsArguments.Term, "гиленхол")};


			PersonSearchInfo personSearchInfo = connector.ExecuteAPI<PersonSearchInfo> (command, typeOfReturnedValueByAPI, argumentsOfCommand);


			Console.WriteLine ("Hi");
		}
	}
}
