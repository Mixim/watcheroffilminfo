using System;
using Gtk;
using ExtensionsLib;
using WatcherOfFilmInfoLib;
using System.Threading;

namespace WatcherOfFilmInfo
{
	public partial class MainWindow: Gtk.Window
	{
		#region Необходимые поля и свойства.
			
			/// <summary>
			/// Соединение с хранилищем данных.
			/// </summary>
			ConnectorToDatastore Connector{ get; set; }

			/// <summary>
			/// Поток для поиска.
			/// </summary>
			Thread SearchingThread{ get; set; }

			/// <summary>
			/// Ссылка на страницу фильма.
			/// </summary>
			/// <value>The url_button.</value>
			LinkButton url_button{ get; set; }

		#endregion

		/// <summary>
		/// Инициализирует класс <see cref="MainWindow"/>.
		/// </summary>
		public MainWindow (): base (Gtk.WindowType.Toplevel)
		{
			Build ();
			String datastoresURL = "http://api.cinemate.cc";
			this.Connector = new ConnectorToDatastore (datastoresURL, AuthorizationConstants.ApiKeyForCinemate);
			url_button = new LinkButton("", "");
			url_button.SetAlignment (0, 0);
			additionalInfo_table.Attach(url_button, 1, 2, 7, 8);
		}

		/// <summary>
		/// Обработчик события удаления формы.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="a">The alpha component.</param>
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}

		protected void SearchingThreadMethod()
		{
			CinemateCcAPICommands command = CinemateCcAPICommands.MovieSearch;
			TypesOfReturnedValueByAPI typeOfReturnedValueByAPI = TypesOfReturnedValueByAPI.JSON;
			ArgumentOfCinemateCcApiCommands[] argumentsOfCommand = { new ArgumentOfCinemateCcApiCommands(NamesOfCinemateCcAPICommandsArguments.Term, search_entry.Text) };
			MovieListInfo movieListInfo = this.Connector.ExecuteAPI<MovieListInfo> (command, typeOfReturnedValueByAPI, argumentsOfCommand);
			Gtk.Application.Invoke(delegate
			                       		{
											this.search_combobox.SetGenericModel(movieListInfo.Movies, 
				                                     movieListInfo.Movies.CalculateLayout);
										}
									);
		}

		public void CalculateLayout(CellLayout LayoutOfCell, CellRenderer Cell, TreeModel Model, TreeIter Iter)
		{
			(Cell as CellRendererText).Text = Model.GetValue (Iter, 0).ToString();
		}

		protected void OnSearchEntryChanged (object sender, EventArgs e)
		{
			if (this.SearchingThread != null)
			{
				this.SearchingThread.Abort();
			}
			this.SearchingThread = new Thread (new ThreadStart (this.SearchingThreadMethod));
			this.SearchingThread.Start ();
		}

		protected void OnSearchComboboxChanged (object sender, EventArgs e)
		{
			TreeIter activeIter;
			search_combobox.GetActiveIter(out activeIter);
			MovieGenericInfo selectedMovie = (MovieGenericInfo) search_combobox.Model.GetValue (activeIter, 0);

			CinemateCcAPICommands command = CinemateCcAPICommands.Movie;
			TypesOfReturnedValueByAPI typeOfReturnedValueByAPI = TypesOfReturnedValueByAPI.JSON;
			ArgumentOfCinemateCcApiCommands[] argumentsOfCommand = { new ArgumentOfCinemateCcApiCommands(NamesOfCinemateCcAPICommandsArguments.Id, selectedMovie.Id.ToString()) };


			MovieInfo movieInfo = this.Connector.ExecuteAPI<MovieInfo> (command, typeOfReturnedValueByAPI, argumentsOfCommand);
				title_label.Text=movieInfo.RussianTitle;
				year_entry.Text=movieInfo.Year.ToString();
				countries_entry.Text = movieInfo.Countries.ToString();
				director_entry.Text=movieInfo.Director.ToString();
				worldReleaseDate_entry.Text=movieInfo.WorldReleaseDate;
				russiaReleaseDate_entry.Text=movieInfo.RussiaReleaseDate;
				runtime_entry.Text=movieInfo.Runtime.ToString();
				russianTitle_entry.Text=movieInfo.RussianTitle;
				originalTitle_entry.Text=movieInfo.OriginalTitle;
				cast_entry.Text=movieInfo.Cast.ToString();
				genre_entry.Text=movieInfo.Genre.ToString();
				if(movieInfo.Kinopoisk!=null)
				{
					kinopoiskRating_entry.Text=movieInfo.Kinopoisk.Rating;
					kinopoiskVotes_entry.Text=movieInfo.Kinopoisk.Votes;
				}
				if(movieInfo.Imdb!=null)
				{
					imdbRating_entry.Text=movieInfo.Imdb.Rating;
					imdbVotes_entry.Text=movieInfo.Imdb.Votes;
				}
				trailer_entry.Text=movieInfo.Trailer;
				url_button.Label=url_button.Uri=movieInfo.Url;
				description_textview.Buffer.Text=movieInfo.Description;
		}
	}
}