using System;
using Gtk;
using WatcherOfFilmInfoLib;
using System.Collections.Generic;

namespace WatcherOfFilmInfoLib
{
	/// <summary>
	/// Дополнительные методы для <see cref="IEnumerable<MovieGenericInfo>"/>.
	/// </summary>
	public static class MovieGenericInfoExtensions
	{
		/// <summary>
		/// Вычислить расположение объекта.
		/// </summary>
		/// <param name="GenericInfos">Generic infos.</param>
		/// <param name="LayoutOfCell">Layout of cell.</param>
		/// <param name="Cell">Cell.</param>
		/// <param name="Model">Model.</param>
		/// <param name="Iter">Iter.</param>
		public static void CalculateLayout(this IEnumerable<MovieGenericInfo> GenericInfos, CellLayout LayoutOfCell, CellRenderer Cell, TreeModel Model, TreeIter Iter)
		{
			//MovieGenericInfo movieGenericInfo = Model.GetValue (Iter, 0) as MovieGenericInfo;

			//(Cell as CellRendererText).Text = movieGenericInfo.ToString();
			(Cell as CellRendererText).Text = Model.GetValue (Iter, 0).ToString ();
		}
	}
}

