using System;
using Gtk;
using System.Collections.Generic;

namespace ExtensionsLib
{
	/// <summary>
	/// Дополнительные методы для интерфейса <see cref="System.Collections.Generic.IEnumerable{T}"/>
	/// </summary>
	public static class IEnumerableExtensions
	{
		/// <summary>
		/// Создает <see cref="Gtk.ListStore"/> из объекта <see cref="System.Collections.Generic.IEnumerable{T}"/>.
		/// </summary>
		/// <returns>ListStore.</returns>
		/// <param name="Parent">Элемент-источник.</param>
		public static ListStore ToListStore<T>(this IEnumerable<T> Parent)
		{
			ListStore returnedValue = new ListStore(typeof (object));

			if (Parent != null) 
			{
				foreach (T node in Parent) 
				{
					returnedValue.AppendValues (node);
				}
			}

			return returnedValue;
		}
	}
}

