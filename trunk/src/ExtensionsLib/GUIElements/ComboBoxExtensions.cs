using System;
using Gtk;
using System.Collections.Generic;

namespace ExtensionsLib
{
	public static class ComboBoxExtensions
	{
		/// <summary>
		/// Задает в качестве модели <see cref="Gtk.ComboBox"/> переданный перечислитель.
		/// </summary>
		/// <param name="Parent">Родительский объект.</param>
		/// <param name="Model">Устанавливаемая модель.</param>
		/// <param name="LayoutFuncForModel">Функция для привязки данных.</param>
		public static void SetGenericModel<T>(this ComboBox Parent, IEnumerable<T> Model, CellLayoutDataFunc LayoutFuncForModel)
		{
			ListStore store = Model.ToListStore ();
			CellRendererText genericClassCell = new CellRendererText();
			Parent.Clear ();
			Parent.PackStart(genericClassCell, true);
			Parent.SetCellDataFunc(genericClassCell, LayoutFuncForModel);
			TreeIter treeIter;
			store.GetIterFirst (out treeIter);
			Parent.Model = store;
		}
	}
}

