using System;
using System.Reflection;

namespace Common
{
	/// <summary>
	/// Утилиты, необходимые для работы с энумераторами.
	/// </summary>
	public class EnumsUtilities
	{
		/// <summary>
		/// Получить строковое представление энумератора.
		/// </summary>
		/// <returns>Строковое представление энумератора.</returns>
		/// <param name="EnumsValue">Энумератор для получения строкового представления.</param>
		public static string GetStringValue(Enum EnumsValue)
		{
			Type type = EnumsValue.GetType();
			FieldInfo fieldInfo = type.GetField(EnumsValue.ToString());
			StringValueAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
			return attribs.Length > 0 ? attribs[0].StringValue : String.Empty;
		}

        /// <summary>
        /// Получить элемент ссылки.
        /// </summary>
        /// <returns>Элемент ссылки.</returns>
        /// <param name="EnumsValue">Энумератор для получения элемента ссылки.</param>
        public static string GetUrlElementValue(Enum EnumsValue)
        {
            Type type = EnumsValue.GetType();
            FieldInfo fieldInfo = type.GetField(EnumsValue.ToString());
            UrlElementAttribute[] attribs = fieldInfo.GetCustomAttributes(typeof(UrlElementAttribute), false) as UrlElementAttribute[];
            return attribs.Length > 0 ? attribs[0].UrlElement : String.Empty;
        }
	}
}

