using System;

namespace Common
{
	/// <summary>
	/// Аттрибут для представления части URL.
	/// </summary>
	public class UrlElementAttribute : Attribute
	{
		#region Свойства.

			/// <summary>
			/// Часть URL.
			/// </summary>
			public string UrlElement{ get; set; }

		#endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
		public UrlElementAttribute ()
		{
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="UrlElement">Часть URL.</param>
		public UrlElementAttribute(string UrlElement)
		{
			this.UrlElement = UrlElement;
		}
	}
}

