using System;

namespace Common
{
	/// <summary>
	/// Строковое представление энумераторов.
	/// </summary>
	public class StringValueAttribute : Attribute
	{
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА StringValueAttribute.
			
			/// <summary>
			/// Строковое представление энумератора.
			/// </summary>
			/// <value>The string value.</value>
			public String StringValue{get;set;}

		#endregion

		/// <summary>
		/// Инициализирует новый экземпляр класса <see cref="WatcherOfFilmInfoLib.StringValueAttribute"/>.
		/// </summary>
		/// <param name="StringValue">Строковое представление энумератора.</param>
		public StringValueAttribute(String StringValue)
		{
			this.StringValue = StringValue;
		}
	}
}

