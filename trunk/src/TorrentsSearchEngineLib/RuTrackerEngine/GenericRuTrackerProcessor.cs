using System;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TorrentsSearchEngineLib
{
	/// <summary>
	/// Обобщенный поисковик для портала http://rutracker.org .
	/// </summary>
	public class GenericRuTrackerProcessor
	{
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ.

			/// <summary>
			/// Ссылка на страницу авторизации.
			/// </summary>
			protected const string LoginUrl = "http://login.rutracker.org/forum/login.php";

			/// <summary>
			/// Ссылка на страницу поиска.
			/// </summary>
			protected const string SearchUrlFormat = "http://rutracker.org/forum/tracker.php?nm={0}";

			/// <summary>
			/// Ссылка на найденный торрент.
			/// </summary>
			protected const string FoundDistributionUrlFormat="http://rutracker.org/forum/{0}";

			/// <summary>
			/// Имя параметра с логином для авторизации.
			/// </summary>
			protected const string UsernameParamName = "login_username";

			/// <summary>
			/// Имя параметра с паролем для авторизации.
			/// </summary>
			protected const string PasswordParamName = "login_password";

			/// <summary>
			/// Имя параметра с признаком авторизации.
			/// </summary>
			protected const string AuthorizationParamName = "login";

			/// <summary>
			/// Значение параметра с признаком авторизации.
			/// </summary>
			protected const string AuthorizationParamValue = "%C2%F5%EE%E4";

			/// <summary>
			/// Формат не последнего параметра в Http-запросе.
			/// </summary>
			protected const string NotLastParamFormat = "{0}={1}&";

			/// <summary>
			/// Формат последнего параметра в Http-запросе.
			/// </summary>
			protected const string LastParamFormat = "{0}={1}";

			/// <summary>
			/// Имя параметра с ключевыми словами для поиска.
			/// </summary>
			protected const string KeywordParamName = "nm";

			/// <summary>
			/// POST-метод запроса.
			/// </summary>
			protected const string PostWebMethod = "POST";

			/// <summary>
			/// Имя cookie-параметра с ключом авторизации.
			/// </summary>
			protected const string AuthorizationCookieName = "bb_data";

			/// <summary>
			/// Используемая кодировка сервера при ответах на Web-запросы.
			/// </summary>
			protected const string UsedEncodingOFWebResponse = "windows-1251";

			/// <summary>
			/// Заголовок Web-запросов Content-Type.
			/// </summary>
			protected const string ContentTypeHeaderValue = "application/x-www-form-urlencoded";

			/// <summary>
			/// Заголовок Web-запросов Accept.
			/// </summary>
			protected const string AcceptHeaderValue = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
			
			/// <summary>
			/// Заголовок Web-запросов Accept-Language.
			/// </summary>
			protected const string AcceptLanguageHeaderValue = "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4";

			/// <summary>
			/// Заголовок Web-запросов Accept-Encoding.
			/// </summary>
			protected const string AcceptEncodingHeaderValue = "gzip, deflate";

			/// <summary>
			/// Заголовок Web-запросов User-Agent.
			/// </summary>
			protected const string UserAgentHeaderValue = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36";

			/// <summary>
			/// Текст исключения при некорректном формате ответа, полученного от сервера.
			/// </summary>
			protected const string InvalidProcessingPageContentExceptionFormat = "Некорректный формат полученной от сервера страницы: '{0}'";

			/// <summary>
			/// Паттерн для извлечения со страницы элемента с основным содержимым.
			/// </summary>
			protected const string MainContentPattern = "<tbody>(?<mainContent>.*)</tbody>";

			/// <summary>
			/// Имя элемента с основным содержимым в паттерне.
			/// </summary>
			protected const string MainContentPatternName = "mainContent";

			/// <summary>
			/// Паттерн для извлечения со страницы элемента с раздачей.
			/// </summary>
			protected const string DistributionContentPattern = "<tr[^>]*>.*?" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?" +
																	"<div[^>]*>.*?" +
																		"<a[^>]*href=\"(?<url>.*?)\">(?<title>.*?)</a>\\s*" +
																	"</div>\\s*" +
																	"<div[^>]*>.*?"+
																	"</div>\\s*" +
																"</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
																"<td[^>]*>.*?</td>\\s*" +
														   "</tr>";
            
            /// <summary>
            /// Имя элемента с ссылкой на раздачу в паттерне.
            /// </summary>
			protected const string DistributionUrlPatternName = "url";

            /// <summary>
            /// Имя элемента с заголовком раздачи в паттерне.
            /// </summary>
			protected const string DistributionTitlePatternName = "title";

            /// <summary>
            /// Имя Cookie с Id торрента.
            /// </summary>
            protected const string TorrentIdCookieName = "bb_dl";

            /// <summary>
            /// Подмножество URI на сервере RuTracker, к которому применяется Id торрента.
            /// </summary>
            protected const string TorrentIdCookiePath = "/";

            /// <summary>
            /// Интернет-домен, для которого действует Cookie с Id торрента.
            /// </summary>
            protected const string TorrentIdCookieDomain = ".rutracker.org";

            /// <summary>
            /// Формат сохраняемого торрента файла.
            /// </summary>
            protected const string TorrentFileNameFormat = "downloadedTorrent-{0}.torrent";

		#endregion

		#region НЕОБХОДИМЫЕ ПОЛЯ И СВОЙСТВА.

			/// <summary>
			/// Имя пользователя.
			/// </summary>
			public string Username{ get; set;}

			/// <summary>
			/// Пароль.
			/// </summary>
			public string Password{ get; set;}

			/// <summary>
			/// Cookie авторизации.
			/// </summary>
			protected Cookie AuthorizationCookie{ get; set;}

			/// <summary>
			/// Cookie загрузки файлов.
			/// </summary>
			protected Cookie DownloadCookie{ get; set;}

		#endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
		protected GenericRuTrackerProcessor ()
		{
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="Username">Имя пользователя.</param>
		/// <param name="Password">Пароль.</param>
		public GenericRuTrackerProcessor(string Username, string Password)
		{
			this.Username = Username;
			this.Password = Password;
		}

		/// <summary>
		/// Получить необходимую кодировку.
		/// </summary>
		/// <returns>The required encoding.</returns>
		protected Encoding GetRequiredEncoding()
		{
			return new ASCIIEncoding ();
		}

		/// <summary>
		/// Посчитать длинну параметра для сетевого потока.
		/// </summary>
		/// <returns>Длинна параметра для сетевого потока.</returns>
		/// <param name="ParamName">Имя параметра.</param>
		/// <param name="ParamValue">Значение параметра.</param>
		/// <param name="IsLast">Является ли текущий параметр последним.</param>
		protected int CalcParamLength(string ParamName, string ParamValue, bool IsLast)
		{
			string postData;
			if (IsLast == true)
			{
				postData = string.Format (GenericRuTrackerProcessor.LastParamFormat, ParamName, ParamValue);
			}
			else
			{
				postData = string.Format (GenericRuTrackerProcessor.NotLastParamFormat, ParamName, ParamValue);
			}
			Encoding encodingForCalcBytes = this.GetRequiredEncoding ();
			return encodingForCalcBytes.GetByteCount (postData);
		}

		/// <summary>
		/// Вставить параметр в сетевой поток.
		/// </summary>
		/// <param name="ProcessingStream">Обрабатываемый поток.</param>
		/// <param name="ParamName">Имя параметра.</param>
		/// <param name="ParamValue">Значение параметра.</param>
		/// <param name="IsLast">Является ли текущий параметр последним.</param>
		protected void InsertParamToNetworkStream(ref Stream ProcessingStream, string ParamName, string ParamValue, bool IsLast)
		{
			string postData;
			if (IsLast == true)
			{
				postData = string.Format (GenericRuTrackerProcessor.LastParamFormat, ParamName, ParamValue);
			}
			else
			{
				postData = string.Format (GenericRuTrackerProcessor.NotLastParamFormat, ParamName, ParamValue);
			}
			Encoding encodingForInsertingBytes = this.GetRequiredEncoding ();
			byte[] insertingByte = encodingForInsertingBytes.GetBytes (postData);
			ProcessingStream.Write (insertingByte, 0, insertingByte.Length);
		}

		/// <summary>
		/// Подготовить Web-запрос.
		/// </summary>
		/// <returns>Web-запрос.</returns>
		/// <param name="UrlForWebRequest">URL для Web-запроса.</param>
		/// <param name="WebMethodName">Имя Web-метода.</param>
		protected HttpWebRequest PrepareWebRequest(string UrlForWebRequest, string WebMethodName)
		{
			HttpWebRequest returnedValue;

			returnedValue = (HttpWebRequest)WebRequest.Create (UrlForWebRequest);
			returnedValue.Method = WebMethodName;

			returnedValue.ContentType = GenericRuTrackerProcessor.ContentTypeHeaderValue;

			returnedValue.AllowAutoRedirect = false;
			returnedValue.Accept = GenericRuTrackerProcessor.AcceptHeaderValue;

			WebHeaderCollection myWebHeaderCollection = returnedValue.Headers;
			myWebHeaderCollection.Add(HttpRequestHeader.AcceptLanguage,GenericRuTrackerProcessor.AcceptLanguageHeaderValue);
			myWebHeaderCollection.Add (HttpRequestHeader.AcceptEncoding, GenericRuTrackerProcessor.AcceptEncodingHeaderValue);
			returnedValue.UserAgent = GenericRuTrackerProcessor.UserAgentHeaderValue;

			returnedValue.ServicePoint.Expect100Continue = false;

			returnedValue.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

			return returnedValue;
		}

		/// <summary>
		/// Инициализировать соединение.
		/// </summary>
		/// <returns>Результат авторизации.</returns>
		public bool InitConnection()
		{
			bool returnedValue;
			HttpWebRequest requestForInit = this.PrepareWebRequest (GenericRuTrackerProcessor.LoginUrl, GenericRuTrackerProcessor.PostWebMethod);
			requestForInit.CookieContainer = new CookieContainer ();

			Stream requestStream = requestForInit.GetRequestStream ();

			try
			{
				this.InsertParamToNetworkStream (ref requestStream, GenericRuTrackerProcessor.UsernameParamName, this.Username, false);
				this.InsertParamToNetworkStream (ref requestStream, GenericRuTrackerProcessor.PasswordParamName, this.Password, false);
				this.InsertParamToNetworkStream (ref requestStream, GenericRuTrackerProcessor.AuthorizationParamName, GenericRuTrackerProcessor.AuthorizationParamValue, true);

				requestStream.Close ();

				using (HttpWebResponse responseOfInit = (HttpWebResponse)requestForInit.GetResponse ())
				{
					this.AuthorizationCookie = responseOfInit.Cookies [GenericRuTrackerProcessor.AuthorizationCookieName];
					returnedValue = this.AuthorizationCookie != null;
					responseOfInit.Close();
				}
			}
			finally
			{
				requestStream.Close ();
				requestStream.Dispose ();
			}

			return returnedValue;
		}

		/// <summary>
		/// Разобрать результаты поиска раздач.
		/// </summary>
		/// <returns>Найденные раздачи.</returns>
		/// <param name="SearchHtmlResult">Результаты поиска раздач в Html-формате.</param>
		protected LinkedList<RuTrackerFoundDistribution> PrepareSearchResult(string SearchHtmlResult)
		{
			LinkedList<RuTrackerFoundDistribution> returnedValue = new LinkedList<RuTrackerFoundDistribution> ();
			string mainContent;
			Regex mainContentGetter = new Regex (GenericRuTrackerProcessor.MainContentPattern, RegexOptions.Singleline | RegexOptions.Compiled);
			Match mainContentMatch = mainContentGetter.Match (SearchHtmlResult);
			if (mainContentMatch.Success == true)
			{
				mainContent = mainContentMatch.Value;
				Regex torrentsContentGetter = new Regex (GenericRuTrackerProcessor.DistributionContentPattern, RegexOptions.Singleline | RegexOptions.Compiled);
				MatchCollection torrentsContentsMatchCol = torrentsContentGetter.Matches (mainContent);
				foreach (Match torrentsContent in torrentsContentsMatchCol)
				{
					returnedValue.AddLast (new RuTrackerFoundDistribution (torrentsContent.Groups [GenericRuTrackerProcessor.DistributionTitlePatternName].Value,
					                                            		new Uri(string.Format(GenericRuTrackerProcessor.FoundDistributionUrlFormat, 
					                      													torrentsContent.Groups [GenericRuTrackerProcessor.DistributionUrlPatternName].Value)
					        											)
					                                              )
					                       );
				}
			}
			else
			{
				throw new InvalidDataException (string.Format (GenericRuTrackerProcessor.InvalidProcessingPageContentExceptionFormat,
				                                               SearchHtmlResult));
			}

			return returnedValue;
		}

		/// <summary>
		/// Найти раздачи по ключевому слову.
		/// </summary>
		/// <param name="KeyWord">Ключевое слово.</param>
		public LinkedList<RuTrackerFoundDistribution> Search(string KeyWord)
		{
			LinkedList<RuTrackerFoundDistribution> returnedValue;
			string searchStrResult;
			HttpWebRequest searchRequest = this.PrepareWebRequest (string.Format(GenericRuTrackerProcessor.SearchUrlFormat, WebUtility.UrlEncode(KeyWord)),
			                                                       GenericRuTrackerProcessor.PostWebMethod);

			searchRequest.CookieContainer = new CookieContainer ();
			searchRequest.CookieContainer.Add (this.AuthorizationCookie);

			Stream requestStream = searchRequest.GetRequestStream ();
			try
			{
				this.InsertParamToNetworkStream(ref requestStream, GenericRuTrackerProcessor.KeywordParamName, KeyWord, true);
				requestStream.Close();

				using(HttpWebResponse searchResponse = (HttpWebResponse)searchRequest.GetResponse())
				{
					var encoding = Encoding.GetEncoding(GenericRuTrackerProcessor.UsedEncodingOFWebResponse);
					using(StreamReader responseStream = new StreamReader(searchResponse.GetResponseStream(), encoding))
					{
						searchStrResult = responseStream.ReadToEnd();
					}
				}
			}
			finally
			{
				requestStream.Dispose ();
			}

			returnedValue = this.PrepareSearchResult (searchStrResult);

			return returnedValue;
		}


		/// <summary>
		/// Скачать торрент.
		/// </summary>
		/// <returns>Путь к торрент-файлу.</returns>
		/// <param name="Distribution">Раздача, для которой необходимо скачать торрент-файл.</param>
		public string DownloadTorrent(RuTrackerFoundDistribution Distribution)
		{
			string returnedValue;
            string relativePath;
			CookieContainer cookieContainer = new CookieContainer ();
			cookieContainer.Add (this.AuthorizationCookie);
            cookieContainer.Add (new Cookie (GenericRuTrackerProcessor.TorrentIdCookieName, 
                                             Distribution.TorrentId.ToString(),
                                             GenericRuTrackerProcessor.TorrentIdCookiePath,
                                             GenericRuTrackerProcessor.TorrentIdCookieDomain));
			using (CookieWebClient torrentDownloader=new CookieWebClient(cookieContainer))
			{
                torrentDownloader.Headers.Add (HttpRequestHeader.Referer, Distribution.Url.AbsoluteUri);
                relativePath = string.Format (GenericRuTrackerProcessor.TorrentFileNameFormat, Guid.NewGuid());
                torrentDownloader.DownloadFile (Distribution.TorrentUrl, relativePath);
			}
            returnedValue = Path.GetFullPath (relativePath);
			return returnedValue;
		}
	}
}

