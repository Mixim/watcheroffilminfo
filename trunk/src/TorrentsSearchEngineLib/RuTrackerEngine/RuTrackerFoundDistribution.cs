using System;
using System.Text.RegularExpressions;

namespace TorrentsSearchEngineLib
{
	public class RuTrackerFoundDistribution : FoundDistribution
	{
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ.

			/// <summary>
			/// Паттерн для извлечения Id торрента из ссылки на раздачу.
			/// </summary>
			protected const string TorrentIdPattern = "\\.php\\?t=(?<torrentId>\\d+)";

			/// <summary>
			/// Имя элемента с Id торрента в паттерне.
			/// </summary>
			protected const string TorrentIdPatternName = "torrentId";

			/// <summary>
			/// Текст исключения при некорректном формате ссылки.
			/// </summary>
			protected const string InvalidUrlExceptionFormat = "Полученная ссылка на раздачу: \"{0}\" не соответствует формату: \"{1}\"";

			/// <summary>
			/// Формат ссылки для скачивания торрента.
			/// </summary>
			protected const string TorrentUrlFormat = "http://dl.rutracker.org/forum/dl.php?t={0}";

		#endregion

		#region НЕОБХОДИМЫЕ ПОЛЯ И СВОЙСТВА КЛАССА.

			public ulong TorrentId
			{
				get
				{
                    Match idMatch = Regex.Match (this.Url.AbsoluteUri, RuTrackerFoundDistribution.TorrentIdPattern);
                    return ulong.Parse (idMatch.Groups [RuTrackerFoundDistribution.TorrentIdPatternName].Value);
				}
			}

			public Uri TorrentUrl
			{
				get
				{
                    Uri returnedValue;
                    returnedValue = new Uri (string.Format(RuTrackerFoundDistribution.TorrentUrlFormat, this.TorrentId));
					return returnedValue;
				}
			}

		#endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="Title">Заголовок.</param>
		/// <param name="Url">Ссылка на раздачу.</param>
		public RuTrackerFoundDistribution(string Title, Uri Url)
			: base(Title, Url)
		{
			if (Regex.IsMatch (Url.AbsoluteUri, RuTrackerFoundDistribution.TorrentIdPattern) == false)
			{
				throw new ArgumentException (string.Format (RuTrackerFoundDistribution.InvalidUrlExceptionFormat, 
				                                            Url.AbsoluteUri, RuTrackerFoundDistribution.TorrentIdPattern));
			}
		}
	}
}

