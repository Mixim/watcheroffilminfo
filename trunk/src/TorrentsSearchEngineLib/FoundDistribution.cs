using System;
using System.Web;

namespace TorrentsSearchEngineLib
{
	/// <summary>
	/// Найденная раздача.
	/// </summary>
	public abstract class FoundDistribution
	{
		#region НЕОБХОДИМЫЕ ПОЛЯ И СВОЙСТВА КЛАССА.

			/// <summary>
			/// Заголовок.
			/// </summary>
            public virtual string Title {get; protected set;}

			/// <summary>
			/// Ссылка на раздачу.
			/// </summary>
            public virtual Uri Url {get; protected set;}

		#endregion

        /// <summary>
        /// Конструктор.
        /// </summary>
        protected FoundDistribution()
        {
        }

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="Title">Заголовок.</param>
		/// <param name="Url">Ссылка на раздачу.</param>
		public FoundDistribution(string Title, Uri Url)
		{
			this.Title = Title;
			this.Url = Url;
		}
	}
}

