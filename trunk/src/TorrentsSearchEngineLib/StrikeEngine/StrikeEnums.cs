using System;
using Common;

namespace TorrentsSearchEngineLib
{
	/// <summary>
	/// Категории доступного контента протокола Strike API v2.
	/// </summary>
	public enum StrikeCategories
	{
		/// <summary>
		/// Аниме.
		/// </summary>
		[UrlElement("Anime")]
		Anime = 0,

		/// <summary>
		/// Приложения.
		/// </summary>
        [UrlElement("Applications")]
		Applications = 1,

		/// <summary>
		/// Книги.
		/// </summary>
		[UrlElement("Books")]
		Books = 2,

		/// <summary>
		/// Игры.
		/// </summary>
		[UrlElement("Games")]
		Games = 3,

		/// <summary>
		/// Фильмы.
		/// </summary>
		[UrlElement("Movies")]
		Movies = 4,

		/// <summary>
		/// Музыка.
		/// </summary>
		[UrlElement("Music")]
		Music = 5,

		/// <summary>
		/// Другое.
		/// </summary>
		[UrlElement("Other")]
		Other = 6,

		/// <summary>
		/// ТВ.
		/// </summary>
		[UrlElement("TV")]
		TV = 7,

		[UrlElement("XXX")]
		XXX = 8
	}

	/// <summary>
	/// Подкатегории доступного контента протокола Strike API v2.
	/// </summary>
	public enum StrikeSubCategories
	{
		[UrlElement("")]
		HighResMovies = 0,

		[UrlElement("")]
		Hentai = 1,

		[UrlElement("")]
		HDVideo = 2,

		[UrlElement("")]
		Handheld = 3,

		[UrlElement("")]
		Games = 4,

		[UrlElement("")]
		Fiction = 5,

		[UrlElement("")]
		EnglishTranslated = 6,

		[UrlElement("")]
		Ebooks = 7,

		[UrlElement("")]
		DubbedMovies = 8,

		[UrlElement("")]
		Documentary = 9,

		[UrlElement("")]
		Concerts = 10,

		[UrlElement("")]
		Comics = 11,

		[UrlElement("")]
		Books = 12,

		[UrlElement("")]
		Bollywood = 13,

		[UrlElement("")]
		AudioBooks = 14,

		[UrlElement("")]
		Asian = 15,

		[UrlElement("")]
		AnimeMusicVideo = 16,

		[UrlElement("")]
		Animation = 17,

		[UrlElement("")]
		Android = 18,

		[UrlElement("")]
		Academic = 19,

		[UrlElement("")]
		AAC = 20,

		[UrlElement("")]
		Movies3D = 21,

		[UrlElement("")]
		XBox360 = 22,

		[UrlElement("")]
		Windows = 23,

		[UrlElement("")]
		Wii = 24,

		[UrlElement("")]
		Wallpapers = 25,

		[UrlElement("")]
		Video = 26,

		[UrlElement("")]
		Unsorted = 27,

		[UrlElement("")]
		UNIX = 28,

		[UrlElement("")]
		UltraHD = 29,

		[UrlElement("")]
		Tutorials = 30,

		[UrlElement("")]
		Transcode = 31,

		[UrlElement("")]
		Trailer = 32,

		[UrlElement("")]
		Textbooks = 33,

		[UrlElement("")]
		Subtitles = 34,

		[UrlElement("")]
		Soundtrack = 35,

		[UrlElement("")]
		SoundClips = 36,

		[UrlElement("")]
		RadioShows = 37,

		[UrlElement("")]
		PSP = 38,

		[UrlElement("")]
		PS3 = 39,

		[UrlElement("")]
		PS2 = 40,

		[UrlElement("")]
		Poetry = 41,

		[UrlElement("")]
		Pictures = 42,

		[UrlElement("")]
		PC = 43,

		[UrlElement("")]
		OtherXXX = 44,

		[UrlElement("")]
		OtherTV = 45,

		[UrlElement("")]
		OtherMusic = 46,

		[UrlElement("")]
		OtherMovies = 47,

		[UrlElement("")]
		OtherGames = 48,

		[UrlElement("")]
		OtherBooks = 49,

		[UrlElement("")]
		OtherApplications = 50,

		[UrlElement("")]
		OtherAnime = 51,

		[UrlElement("")]
		NonFiction = 52,

		[UrlElement("")]
		Newspapers = 53,

		[UrlElement("")]
		MusicVideos = 54,

		[UrlElement("")]
		Mp3 = 55,

		[UrlElement("")]
		MovieClips = 56,

		[UrlElement("")]
		Magazines = 57,

		[UrlElement("")]
		Mac = 58,

		[UrlElement("")]
		Lossless = 59,

		[UrlElement("")]
		Linux = 60,

		[UrlElement("")]
		Karaoke = 61,

		[UrlElement("")]
		iOS = 62
	}

	/// <summary>
	/// API-команды протокола Strike API v2.
	/// </summary>
	public enum StrikeAPICommands
	{
		/// <summary>
		/// Получить информацию о торренте по его хешу.
		/// </summary>
		GetInfo = 0,

		/// <summary>
		/// Получить информацию по количеству торрентов.
		/// </summary>
		GetCount = 1,

		/// <summary>
		/// Поиск по названию.
		/// </summary>
		SearchByTitle = 2,

		/// <summary>
		/// Поиск по названию и категории.
		/// </summary>
		SearchByTitleAndCategory = 3,

		/// <summary>
		/// Получить описание торрента.
		/// </summary>
		GetDescription = 4,

		/// <summary>
		/// Скачать торрент.
		/// </summary>
		DownloadTorrent = 5
	}
}

