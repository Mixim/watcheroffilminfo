using System;
using Common;
using System.Net;
using System.IO;
using System.Text;
using System.Web;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.Serialization.Json;

namespace TorrentsSearchEngineLib
{
	/// <summary>
	/// Обобщенный поисковик для портала https://getstrike.net .
	/// </summary>
	public class GenericStrikeProcessor
	{
        #region НЕОБХОДИМЫЕ КОНСТАНТЫ.

            /// <summary>
            /// Ссылка на страницу поиска.
            /// </summary>
            protected const string SearchUrl = "https://getstrike.net/api/v2/torrents/search/";

            /// <summary>
            /// POST-метод запроса.
            /// </summary>
            protected const string PostWebMethod = "POST";

            /// <summary>
            /// Формат не последнего параметра в Http-запросе.
            /// </summary>
            protected const string NotLastParamFormat = "{0}={1}&";

            /// <summary>
            /// Формат последнего параметра в Http-запросе.
            /// </summary>
            protected const string LastParamFormat = "{0}={1}";

            /// <summary>
            /// Имя параметра с ключевыми словами для поиска.
            /// </summary>
            protected const string KeywordParamName = "phrase";

            /// <summary>
            /// Имя параметра с категорией для поиска.
            /// </summary>
            protected const string CategoryParamName = "category";

            /// <summary>
            /// Имя параметра с подкатегорией для поиска.
            /// </summary>
            protected const string SubCategoryParamName = "subcategory";

            /// <summary>
            /// Заголовок Web-запросов Content-Type.
            /// </summary>
            protected const string ContentTypeHeaderValue = "application/x-www-form-urlencoded";

            /// <summary>
            /// Заголовок Web-запросов Accept.
            /// </summary>
            protected const string AcceptHeaderValue = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

            /// <summary>
            /// Заголовок Web-запросов Accept-Language.
            /// </summary>
            protected const string AcceptLanguageHeaderValue = "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4";

            /// <summary>
            /// Заголовок Web-запросов Accept-Encoding.
            /// </summary>
            protected const string AcceptEncodingHeaderValue = "gzip, deflate";

            /// <summary>
            /// Заголовок Web-запросов User-Agent.
            /// </summary>
            protected const string UserAgentHeaderValue = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/41.0.2272.76 Chrome/41.0.2272.76 Safari/537.36";

            /// <summary>
            /// Используемая кодировка сервера при ответах на Web-запросы.
            /// </summary>
            protected const string UsedEncodingOfWebResponse = "windows-1251";

            /// <summary>
            /// Формат сохраняемого торрента файла.
            /// </summary>
            protected const string TorrentFileNameFormat = "downloadedTorrent-{0}.torrent";

        #endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
        public GenericStrikeProcessor ()
		{
		}

        /// <summary>
        /// Получить необходимую кодировку.
        /// </summary>
        /// <returns>The required encoding.</returns>
        protected Encoding GetRequiredEncoding()
        {
            return new ASCIIEncoding ();
        }



        /// <summary>
        /// Получить строкове представление категории.
        /// </summary>
        /// <returns>Строковое представление категории.</returns>
        /// <param name="Category">Категория.</param>
        protected string GetCategoryStringElement(StrikeCategories Category)
        {
            string returnedValue;

            returnedValue = EnumsUtilities.GetUrlElementValue (Category);
            return returnedValue;
        }

        /// <summary>
        /// Получить строкове представление подкатегории.
        /// </summary>
        /// <returns>Строковое представление подкатегории.</returns>
        /// <param name="SubCategory">Подкатегория.</param>
        protected string GetSubCategoryStringElement(StrikeSubCategories SubCategory)
        {
            string returnedValue;

            returnedValue = EnumsUtilities.GetUrlElementValue (SubCategory);
            return returnedValue;
        }

        /// <summary>
        /// Подготовить Web-запрос.
        /// </summary>
        /// <returns>Web-запрос.</returns>
        /// <param name="UrlForWebRequest">URL для Web-запроса.</param>
        /// <param name="WebMethodName">Имя Web-метода.</param>
        /// <param name="ParametersForRequest">Параметры для запроса.</param>
        protected HttpWebRequest PrepareWebRequest(string UrlForWebRequest, string WebMethodName, Dictionary<string, string> ParametersForRequest)
        {
            HttpWebRequest returnedValue;

            UriBuilder urlBuilder = new UriBuilder (UrlForWebRequest);
            NameValueCollection paramsCollection = System.Web.HttpUtility.ParseQueryString (urlBuilder.Query);
            foreach(KeyValuePair<string, string> currentParam in ParametersForRequest)
            {
                paramsCollection.Add (currentParam.Key, currentParam.Value);
            }
            urlBuilder.Query = paramsCollection.ToString ();
            returnedValue = (HttpWebRequest)WebRequest.Create (urlBuilder.Uri);
            returnedValue.Method = WebMethodName;


            returnedValue.ContentType = GenericStrikeProcessor.ContentTypeHeaderValue;

            returnedValue.AllowAutoRedirect = false;
            returnedValue.Accept = GenericStrikeProcessor.AcceptHeaderValue;

            WebHeaderCollection myWebHeaderCollection = returnedValue.Headers;
            myWebHeaderCollection.Add(HttpRequestHeader.AcceptLanguage,GenericStrikeProcessor.AcceptLanguageHeaderValue);
            myWebHeaderCollection.Add (HttpRequestHeader.AcceptEncoding, GenericStrikeProcessor.AcceptEncodingHeaderValue);
            returnedValue.UserAgent = GenericStrikeProcessor.UserAgentHeaderValue;

            returnedValue.ServicePoint.Expect100Continue = false;

            returnedValue.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            return returnedValue;
        }

        /// <summary>
        /// Разобрать результаты поиска раздач.
        /// </summary>
        /// <returns>Найденные раздачи.</returns>
        /// <param name="SearchHtmlResult">Результаты поиска раздач в Html-формате.</param>
        protected LinkedList<StrikeFoundDistribution> PrepareSearchResult(string SearchHtmlResult)
        {
            LinkedList<StrikeFoundDistribution> returnedValue = new LinkedList<StrikeFoundDistribution> ();

            DataContractJsonSerializer serializer = new DataContractJsonSerializer (typeof(StrikeFoundDistributionsRoot));

            using (MemoryStream jsonStream = new MemoryStream())
            {
                using(StreamWriter jsonWriter = new StreamWriter(jsonStream))
                {
                    jsonWriter.Write (SearchHtmlResult);
                    jsonWriter.Flush ();
                    jsonStream.Position = 0;
                    returnedValue = (serializer.ReadObject (jsonStream) as StrikeFoundDistributionsRoot).Distributions;
                }
            }

            return returnedValue;
        }

        /// <summary>
        /// Закрывает баг Mono.
        /// </summary>
        protected bool ValidateServerCertificate(object Sender, X509Certificate Certificate, X509Chain Chain, SslPolicyErrors SslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Найти раздачи по ключевому слову.
        /// </summary>
        /// <param name="KeyWord">Ключевое слово.</param>
        /// <param name="Category">Категория.</param>
        /// <param name="SubCategory">Подкатегории.</param>
        public LinkedList<StrikeFoundDistribution> Search(string KeyWord, StrikeCategories Category, StrikeSubCategories SubCategory)
        {
            LinkedList<StrikeFoundDistribution> returnedValue;
            string searchStrResult;
            HttpWebRequest searchRequest;

            Dictionary<string, string> paramsForRequest = new Dictionary<string, string>();
            paramsForRequest.Add(GenericStrikeProcessor.KeywordParamName, KeyWord);
            paramsForRequest.Add(GenericStrikeProcessor.CategoryParamName, this.GetCategoryStringElement(Category));
            paramsForRequest.Add(GenericStrikeProcessor.SubCategoryParamName, this.GetSubCategoryStringElement(SubCategory));
            searchRequest = this.PrepareWebRequest (GenericStrikeProcessor.SearchUrl, GenericStrikeProcessor.PostWebMethod, paramsForRequest);
            //Закрываем баг Mono.
                System.Net.ServicePointManager.ServerCertificateValidationCallback = this.ValidateServerCertificate;
            //========
            using(HttpWebResponse searchResponse = (HttpWebResponse)searchRequest.GetResponse())
            {
                var encoding = Encoding.GetEncoding(GenericStrikeProcessor.UsedEncodingOfWebResponse);
                using(StreamReader responseStream = new StreamReader(searchResponse.GetResponseStream(), encoding))
                {
                    searchStrResult = responseStream.ReadToEnd();
                }
            }

            returnedValue = this.PrepareSearchResult (searchStrResult);

            return returnedValue;
        }

        public string DownloadTorrent(StrikeFoundDistribution Distribution)
        {
            string returnedValue;
            string relativePath;
            using (WebClient torrentDownloader=new WebClient())
            {
                relativePath = string.Format (GenericStrikeProcessor.TorrentFileNameFormat, Guid.NewGuid());
                torrentDownloader.DownloadFile (Distribution.TorrentUrl, relativePath);
            }
            returnedValue = Path.GetFullPath (relativePath);
            return returnedValue;
        }
	}
}

