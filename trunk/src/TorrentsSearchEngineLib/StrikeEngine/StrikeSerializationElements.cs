using System;
using System.Globalization;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;

namespace TorrentsSearchEngineLib
{
    /// <summary>
    /// Корень для сериализации данных, получаемых с помощью StrikeAPI.
    /// </summary>
    [DataContract]
    internal class StrikeFoundDistributionsRoot
    {
        /// <summary>
        /// Код состояния.
        /// </summary>
        [DataMember(Name = "statuscode")]
        public uint StatusCode{ get; set; }

        /// <summary>
        /// Время ответа.
        /// </summary>
        [DataMember(Name = "responsetime")]
        public float ResponseTime{ get; set; }

        /// <summary>
        /// Раздачи.
        /// </summary>
        [DataMember(Name = "torrents")]
        public LinkedList<StrikeFoundDistribution> Distributions{ get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        public StrikeFoundDistributionsRoot()
        {
        }
    }

    /// <summary>
    /// Найденная с помощью StrikeAPI раздача.
    /// </summary>
    [DataContract]
    public class StrikeFoundDistribution : FoundDistribution
    {
        #region НЕОБХОДИМЫЕ КОНСТАНТЫ.

            /// <summary>
            /// Строковый формат даты.
            /// </summary>
            public const string DateFormat = "MMM dd, yyyy";

            /// <summary>
            /// Формат сообщения для исключения при некорректном формате данных.
            /// </summary>
            protected const string InvalidPropertyExceptionFormat = "Значение \"{0}\" для свойства \"{1}\" имеет некорректный формат.";

            /// <summary>
            /// Имя параметра с Id-торрента в регулярных выражениях.
            /// </summary>
            public const string TorrentIdValueName = "torrentId";

            /// <summary>
            /// Паттерн ссылки на раздачу.
            /// </summary>
            public const string UrlPattern = "https://getstrike.net/torrents/(?<torrentId>.*)";

            /// <summary>
            /// Формат ссылки на торрент-файл.
            /// </summary>
            public const string TorrentUrlFormat = "https://getstrike.net/torrents/api/download/{0}.torrent";

        #endregion

        public string TorrentId
        {
            get
            {
                Match torrentIdMatch = Regex.Match (this.Url.ToString(), StrikeFoundDistribution.UrlPattern);
                string torrentId = torrentIdMatch.Groups[StrikeFoundDistribution.TorrentIdValueName].Value;
                return torrentId;
            }
        }

        /// <summary>
        /// Хеш.
        /// </summary>
        [DataMember(Name="torrent_hash")]
        public string Hash{ get; set; }

        [DataMember(Name = "imdbid")]
        public string ImdbId{ get; set; }

        [DataMember(Name = "torrent_title")]
        public override string Title {
            get {
                return base.Title;
            }
            protected set {
                base.Title = value;
            }
        }

        /// <summary>
        /// Категория.
        /// </summary>
        [DataMember(Name = "torrent_category")]
        public string Category{ get; set; }

        /// <summary>
        /// Подкатегория.
        /// </summary>
        [DataMember(Name = "sub_category")]
        public string SubCategory{ get; set; }

        /// <summary>
        /// Количество сидеров.
        /// </summary>
        [DataMember(Name = "seeds")]
        public uint SeedsCount{ get; set; }

        /// <summary>
        /// Количество пиров.
        /// </summary>
        [DataMember(Name = "leeches")]
        public uint PeersCount{ get; set; }

        /// <summary>
        /// Количество файлов в раздаче.
        /// </summary>
        [DataMember(Name = "file_count")]
        public uint FileCount{ get; set; }

        /// <summary>
        /// Размер раздачи.
        /// </summary>
        [DataMember(Name = "size")]
        public ulong Size{ get; set; }

        [DataMember(Name = "upload_date")]
        protected string CreationDateRaw{ get; set; }
        /// <summary>
        /// Дата создания раздачи.
        /// </summary>
        public DateTime CreationDate
        {
            get
            {
                return DateTime.ParseExact (this.CreationDateRaw, StrikeFoundDistribution.DateFormat, CultureInfo.InvariantCulture);
            }
            set
            {
                this.CreationDateRaw = value.ToString (StrikeFoundDistribution.DateFormat, CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Имя пользователя, создавшего раздачу.
        /// </summary>
        [DataMember(Name = "uploader_username")]
        public string UploaderUsername{ get; set; }

        [DataMember(Name = "page")]
        public override Uri Url {
            get
            {
                return base.Url;
            }
            protected set
            {
                if (!Regex.IsMatch (value.ToString(), StrikeFoundDistribution.UrlPattern))
                {
                    throw new FormatException (string.Format (StrikeFoundDistribution.InvalidPropertyExceptionFormat,
                                                            value,
                                                            "Url"));
                }
                base.Url = value;
            }
        }

        /// <summary>
        /// Rss.
        /// </summary>
        [DataMember(Name = "rss_feed")]
        public Uri Rss{ get; set; }

        /// <summary>
        /// Magnet-ссылка на раздачу.
        /// </summary>
        [DataMember(Name = "magnet_uri")]
        public Uri Magnet{ get; set; }

        public Uri TorrentUrl
        {
            get
            {
                Uri returnedValue;
                returnedValue = new Uri (string.Format(StrikeFoundDistribution.TorrentUrlFormat, this.TorrentId));
                return returnedValue;
            }
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        public StrikeFoundDistribution ()
        {
        }
    }
}

