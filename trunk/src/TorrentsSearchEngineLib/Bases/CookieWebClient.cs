using System;
using System.Net;

namespace TorrentsSearchEngineLib
{
	internal class CookieWebClient : WebClient
	{
		#region НЕОБХОДИМЫЕ ПОЛЯ И СВОЙСТВА КЛАССА.
			
			/// <summary>
			/// Используемые Cookie.
			/// </summary>
			public CookieContainer UsedCookies { get; private set; }

		#endregion

		/// <summary>
		/// Конструктор.
		/// </summary>
		public CookieWebClient()
		{
			this.UsedCookies = new CookieContainer();
		}

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="CookieContainer">Коллекция необходимых Cookie.</param>
		public CookieWebClient(CookieContainer CookieContainer)
		{
			this.UsedCookies = CookieContainer;
		}

		protected override WebRequest GetWebRequest(Uri Address)
		{
			HttpWebRequest returnedValue = base.GetWebRequest(Address) as HttpWebRequest;
			if (returnedValue == null)
			{
				return base.GetWebRequest (Address);
			}
			returnedValue.CookieContainer = this.UsedCookies;
			return returnedValue;
		}
	}
}

