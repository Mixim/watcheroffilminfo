using System;
using System.Collections.Generic;
using TorrentsSearchEngineLib;

namespace TorrentsSearchEngineLib_Tester
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			GenericStrikeProcessor processor;

			processor = new GenericStrikeProcessor ();

    		LinkedList<StrikeFoundDistribution> foundDistribs = processor.Search("Hello", StrikeCategories.Movies, StrikeSubCategories.Video);
            Console.WriteLine (foundDistribs.First.Value.TorrentUrl);
			processor.DownloadTorrent (foundDistribs.First.Value);
		}
	}
}
